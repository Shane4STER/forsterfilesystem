CC=gcc
CFLAGS=-c -Wall

all: fsys

fsys: iNode.o fileSys.o
	$(CC) iNode.o directory.o fileSys.o -lm -o fsys

fileSys.o: iNode.o directory.o fileSys.c
	$(CC) $(CFLAGS) fileSys.c

directory.o: directory.c
	$(CC) $(CFLAGS) directory.c

iNode.o: iNode.c
	$(CC) $(CFLAGS) iNode.c

clean:
	rm -rf *.o fsys