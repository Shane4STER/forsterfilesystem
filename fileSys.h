#ifndef FS_DEF
//Constants
#define FS_DEF
#define BLOCKSIZE 512
#define READ_PERM 0x4
#define WRITE_PERM 0x2
#define EXEC_PERM 0x1
#define ROOT_INODE_ADDR 0x1
#define ROOT_DIR_ADDR 0x2
#define TRUE 1
#define FALSE 0

//Global includes
#include <stdio.h>

//Global Typedefs
typedef unsigned int block_t;  //With block size 512 this leaves 512 Bytes x 2^32 = 2TB of addressable space.
typedef long phys_addr_t;

//Global Variables
extern FILE *fileSystem;

//Global structs
typedef struct superBlock {
	char fsLabel[63];
	unsigned int fsSize;
	unsigned int freeSpace;
	block_t root_inode;
	block_t bitmap_start;
	block_t data_start;

} superBlock;

typedef struct directory {
	char name[63]; 			//0x0-0x0F directory name
	short subDirs;
	block_t parent;			//0x100-0x104 link to parent 
	block_t iNode;			//0x105-0x108 link to dir iNode
	block_t subDir[446];	//0x108-0xFFFFFFFF subDir  (MAX sub folders 446)
} dir;

typedef struct iNode {
	char perm;
	char owner;
	char group;
	char locked;
	unsigned int size;
	block_t data[10];
	block_t iDataBlock;
	block_t i2DataBlock;
} iNode; 

//Internal Functions	 
int writeBlock(void*, block_t);
phys_addr_t blockToPhys(block_t);
#endif