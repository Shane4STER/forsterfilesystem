int initialize(char* diskFile, int fSize, char* label);
int createFile(char* fileName, char mode);
int writeFile(char* fileName, FILE *file);
int deleteFile(char* fileName);
int renameFile(char* oldName, char* newName);
int changeMode(char* fileName, char mode);
int makeDir(char* dirName, char mode);
int changeOwner(char* fileName, char owner);
int lockFile(char* fileName);
int unlockFile(char* fileName);