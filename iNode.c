#include <stdlib.h>
#include "fileSys.h"

FILE *fileSystem;
iNode *createInode(char perm, char owner, char group){
	iNode *n = (iNode *) malloc (sizeof(iNode));
	n->perm = perm;
	n->owner = owner;
	n->group = group;
	n->locked = FALSE;
	n->size = BLOCKSIZE;
	int i = 0;
	for (i = 0; i < 11; i++){
		n->data[i] = (block_t) NULL;
	}
	n->iDataBlock = (block_t) NULL;
	n->i2DataBlock = (block_t) NULL;

	return n;
}

iNode *readInode(block_t addr){						//Finish me
	phys_addr_t paddr= blockToPhys(addr);
	iNode *r = (iNode *) malloc (sizeof(iNode));
	fread(r, sizeof(iNode), 1, fileSystem);
	return r;
}




