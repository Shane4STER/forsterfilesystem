#include "fileSys.h"
#include "fileSysInterface.h"
#include "iNode.h"
#include "directory.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>
#define EPSILON 1e-9
FILE *fileSystem;
//Internal functions (mostly defined in fileSys.h)
int writeBlock(void *p, block_t addr){
	if (addr%BLOCKSIZE != 0)
		return -1;
	fseek(fileSystem, (long) addr, SEEK_SET);
	fwrite(p, BLOCKSIZE, 1, fileSystem);
	return 0;
}

phys_addr_t blockToPhys(block_t blockAddr){
	return (phys_addr_t) BLOCKSIZE * blockAddr;
}

int initialize(char* diskFile, int fSize, char* label){
	int nInodeBlocks;
	int nBitmapBlocks;
	int nDataBlocks;
	fileSystem = fopen(diskFile, "w");
	if (fileSystem == NULL){
		perror("Error opening device.");
		exit(1);
	}
	superBlock *b = (superBlock *)malloc(sizeof(superBlock));
	strncpy(b->fsLabel, label, 63);
	b->fsSize = fSize;

	nDataBlocks = floor(8192*(fSize - BLOCKSIZE)/8195) / BLOCKSIZE; //estimate datablock area
	nBitmapBlocks = ceil((nDataBlocks/BLOCKSIZE)+EPSILON); 	//calculate bitmap area
	nInodeBlocks = ceil((nDataBlocks/16)+EPSILON);			//calculate indoe area
	nDataBlocks = (fSize/BLOCKSIZE) - nInodeBlocks - nBitmapBlocks - 1; //calculate accurate datablock area (-1 for superblock)
	
	printf("Type\tBlocks\tBytes\n");
	printf("FS:\t%d\t%d\n", (int) fSize/BLOCKSIZE, fSize);
	printf("Inode:\t%d\t%d\n", nInodeBlocks, nInodeBlocks * BLOCKSIZE);
	printf("Bitmap:\t%d\t%d\n", nBitmapBlocks, nBitmapBlocks * BLOCKSIZE);
	printf("Data:\t%d\t%d\n", nDataBlocks, nDataBlocks * BLOCKSIZE);

	b->root_inode = 1;
	b->bitmap_start = 1 + nInodeBlocks;
	b->data_start = b->bitmap_start + nBitmapBlocks;

	

	return 0;


}
/* Comment to compile
//External functions (defined in fileSysInterface.h)
int createFile(char *fileName, char mode){
	iNode *dNode = readInode(blockToPhys(ROOT_INODE_ADDR));
	dir *current;
	char* dirName;
	do {
		dirName = strtok(fileName, "/"); //Parse Path
		int i;
		for (i = 0; i < current->subDirs; i++){
			if (strcmp(dirName, current->subDir[i]) = 0){
				current = subDir[i];
				break;
			}
			//TODO
		}
		return -1; //subdir not found;
	} while (*dirName != NULL); 
	return 0;
}
*/

int main(void){
	initialize("test", 10000, "test");
	exit(0);
}